// Params:
// Side
// Climate

diag_log "DRO: getting composition map";
_compositionMap = [] call (compile loadFile "sunday_system\compositionMap.sqf");
diag_log format ["DRO: Got %1 compositions", count _compositionMap];
// [composition, size, climates, side, type]

_filterCompositions = {
  // Params:
  // Side
  // Climate

  // Return:
  // [BUNKERS, HEADQUARTERS, CHECKPOINT, OTHER]

  private ["_ret", "_side", "_climates", "_composition", "_cside",
           "_cclimates", "_ctype"];

  _side = _this select 0;
  _climates = _this select 1;
  if ((typeName _climates) != "ARRAY") then {
    _climates = [_climates];
  };

  // BUNKERS, HEADQUARTERS, CHECKPOINTS, OTHER
  _ret = [[], [], [], []];

  {
    _composition = _x;
    _cclass = _composition select 0;
    _cclimates = _composition select 2;
    _cside = _composition select 3;
    _ctype = _composition select 4;
    if (isClass _cclass) then {
      if (isNil "_cside" || {_side == _cside}) then {
        if (count _cclimates == 0 || count (_cclimates arrayIntersect _climates) > 0) then {
          switch (_ctype) do {
            case "BUNKER": {
              (_ret select 0) pushBack _composition;
            };
            case "HEADQUARTERS": {
              (_ret select 1) pushBack _composition;
            };
            case "CHECKPOINT": {
              (_ret select 2) pushBack _composition;
            };
            default {
              (_ret select 3) pushBack _composition;
            };
          };
        };
      };
    };
  } forEach _compositionMap;

  _ret;
};

cmp_averageNormal = {
    // This function returns the average surface normal
    // along its bounding box
    // Three points are sampled (corners and center)

    private ["_object", "_normals", "_boundingBox"];
    _object = _this select 0;
    _boundingBox = boundingBoxReal _object;

    _normals = [
                 _boundingBox select 0,
                 _boundingBox select 1,
                 [0, 0, 0]
               ] apply {surfaceNormal (_x vectorAdd getPos _object)};

    ((_normals select 0) vectorAdd
     (_normals select 1) vectorAdd
     (_normals select 2) vectorMultiply (1 / 3))
};

cmp_clampNormal = {
    // This function clamps _normal to at most _maxdeviation degrees off
    // vertical, while retaining its direction otherwise

    params ["_normal", ["_maxdeviation", 10, [0]]];

    private _minvertical = cos _maxdeviation;
    if (_normal select 2 > _minvertical) then {
        _normal
    } else {
        // Work backwards, so that after _z = _minvertical, vector is normal
        // (This is a lerp, not a slerp, but it doesn't really matter
        //  after normalization)
        _normal = _normal vectorMultiply sqrt (
          (1 - _minvertical * _minvertical) /
          ((_normal select 0) * (_normal select 0) +
           (_normal select 1) * (_normal select 1))
        );
        _normal set [2, _minvertical];
        _normal
    };
};


private ["_bunkers", "_headquarters", "_checkpoints",
         "_emplacements", "_index", "_bunkeri", "_headquartersi",
         "_checkpointsi", "_emplacementsi"];

_filtered = [_this select 0, _this select 1] call _filterCompositions;
cmp_bunkers      = _filtered select 0;
cmp_headquarters = _filtered select 1;
cmp_checkpoints  = _filtered select 2;
cmp_emplacements = _filtered select 3;
cmp_compositions = cmp_bunkers + cmp_headquarters +
                   cmp_checkpoints + cmp_emplacements;

diag_log "DRO: Composition counts:";
diag_log format ["  Bunkers: %1", count cmp_bunkers];
diag_log format ["  Headquarters: %1", count cmp_headquarters];
diag_log format ["  Checkpoints: %1", count cmp_checkpoints];
diag_log format ["  Emplacements: %1", count cmp_emplacements];

cmp_sizemap = [4, 8, 16, 24];

cmp_pickComposition = {
  // Params:
  // Composition array
  // Points available
  // Minimum Size
  // Maximum Size
  // List of allowed types

  // Return:
  // Valid composition
  // nil if none found

  private ["_ary", "_points", "_index", "_cpoints"];
  _ary = _this param [0, cmp_compositions, [[]]];
  _points = _this param [1, 48, [0]];
  _minsize = _this param [2, 0, [0]];
  _maxsize = _this param [3, 3, [0]];
  _types = _this param [4, [], [[]]];

  _filtered = cmp_compositions select {
    (_x select 1) <= _maxsize &&
    (_x select 1) >= _minsize &&
    {count _types == 0 || (_x select 4) in _types} &&
    // 1.25 so we can understaff a bit
    {cmp_sizemap select (_x select 1) <= _points * 1.25};
  };

  if (count _filtered == 0) then {
    nil
  } else {
    selectRandom _filtered;
  }
};

cmp_spawnComposition = {
  /*
    Description:
    Spawn a CfgGroups composition

    Parameter(s):
    _this select 0: CfgGroups entry (Config)
    _this select 1: the group's side (Side)
    _this select 2: the group's starting position (Array)
    _this select 3: (optional) azimuth (Number)

    Returns:
    An array of spawned vehicles
  */

  private ["_group", "_side", "_pos", "_azimuth"];

  _group = _this param [0, [], [configFile]];
  _side = _this param [1, sideUnknown, [sideUnknown]];
  _pos = _this param [2, [], [[]]];
  _azimuth = _this param [3, 0, [0]];

  private ["_positions", "_types", "_dirs", "_position"];

  //Convert a CfgGroups entry to types and positions.
  _positions = [];
  _types = [];
  _dirs = [];

  for "_i" from 0 to ((count _group) - 1) do
  {
    private ["_item"];
    _item = _group select _i;

    if (isClass _item) then
    {
      _types pushBack getText(_item >> "vehicle");
      _position = getArray(_item >> "position");
      _positions pushBack ([_position, -_azimuth] call BIS_fnc_rotateVector2D);
      _dirs pushBack (getNumber(_item >> "dir") + _azimuth);
    };
  };

  private ["_type", "_isMan", "_vehs"];

  _vehs = [];

  //Create the units according to the selected types.
  for "_i" from 0 to ((count _types) - 1) do
  {
    _type = _types select _i;
    _isMan = getNumber(configFile >> "CfgVehicles" >> _type >> "isMan") == 1;

    if (!_isMan) then
    {
      private ["_relPos", "_itemPos", "_dir", "_veh", "_up"];
      _relPos = _positions select _i;
      _itemPos = [(_pos select 0) + (_relPos select 0), (_pos select 1) + (_relPos select 1), _relPos select 2];

      _dir = _dirs select _i;
      _veh = createVehicle [_type, _itemPos, [], 0, "NONE"];
      _veh setVariable ["BIS_enableRandomization", false];
      _veh setDir _dir;
      _veh setPos _itemPos;
      // Straighten out those structures, same as the editor does it
      if (_type isKindOf "House") then {
        _veh setVectorUp [0, 0, 1];
      } else {
        _up = [_veh] call cmp_averageNormal;
        _up = [_up] call cmp_clampNormal;
        _veh setVectorUp (_up);
      };
      _vehs pushBack _veh;
    };
  };

  _vehs
};

cmp_spawnCompositionClear = {
  /*
    Description:
    Spawn a CfgGroups composition, and clear the area of nearby terrain objects

    Parameter(s):
    _this select 0: CfgGroups entry (Config)
    _this select 1: the group's side (Side)
    _this select 2: the group's starting position (Array)
    _this select 3: (optional) azimuth (Number)

    Returns:
    An array of spawned vehicles
  */

  private ["_vehs", "_veh", "_terrainObjects"];
  _vehs = _this call cmp_spawnComposition;
  {
    _veh = _x;
    _terrainObjects = nearestTerrainObjects [getPos _veh, [], sizeOf (typeOf _veh), false, true];
    {
      hideObjectGlobal _x;
    } forEach (_terrainObjects);
  } forEach (_vehs);

  _vehs
};

cmp_isGoodPosition = {
  /*
    Description:
    Tests if the passed in position is empty and on land
    Four points are tested around the circumference of the
    area for the presence of water.

    Parameter(s):
    _this select 0: the position
    _this select 1: the radius to check
    _this select 2: (Optional) an array of blacklisted positions
    _this select 3: (Optional) the blacklist radius

    Return:
    Boolean
  */

  _pos = _this param [0, [], [[]]];
  _radius = _this param [1, 20, [0]];
  _blacklist = _this param [2, [], [[]]];
  _blacklistRadius = _this param [3, 30, [0]];
  _blacklistRadiusSqr = _blacklistRadius * _blacklistRadius;

  (count (_pos isFlatEmpty [-1, -1, 0.2, _radius]) != 0) && {
    {
      if (surfaceIsWater _x) exitWith {false};
      true
    } forEach [
                [(_pos select 0) - _radius, _pos select 1],
                [(_pos select 0) + _radius, _pos select 1],
                [_pos select 0, (_pos select 1) - _radius],
                [_pos select 0, (_pos select 1) + _radius]
              ]
  } && {
    if (count _blacklist == 0) exitWith {true};
    {
      if ((_x distanceSqr _pos) < _blacklistRadiusSqr) exitWith {false};
      true
    } forEach _blacklist;
  }
};

cmp_pickEmptyPosition = {
  /*
    Description:
    Pick an empty position out of an array of possible positions

    Parameter(s):
    _this select 0: the array of possible positions
    _this select 1: the size of the composition to be placed
    _this select 2: (Optional) an array of blacklisted positions
    _this select 3: (Optional) the blacklist radius

    Return:
    The selected position, or nil
  */

  private ["_posary", "_size", "_radius", "_blacklist", "_blacklistRadius"];

  _posary = _this param [0, [], [[]]];
  _size = _this param [1, 0, [0]];
  _radius = 10 * _size + 10;
  _blacklist = _this param [2, [], [[]]];
  _blacklistRadius = _this param [3, _radius * 2, [0]];

  if(count _posary == 0) exitWith {nil};

  for "_try" from 0 to 4 do {
    _pos = selectRandom _posary;
    if ([_pos, _radius, _blacklist, _blacklistRadius] call cmp_isGoodPosition) exitWith {_pos};
    nil
  }
};
