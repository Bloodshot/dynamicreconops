params ["_AOIndex"];

// Find suitable posision
_posArr = [];
_thisPos = [];


_flatPosClose = (((AOLocations select _AOIndex) select 2) select 4);
_thisPos = [_flatPosClose] call sun_selectRemove;
_thisPos = [(_thisPos select 0), (_thisPos select 1), 0];

// Create area marker
_markerName = format["areaMkr%1", floor(random 10000)];
_markerArea = createMarker [_markerName, _thisPos];
_markerArea setMarkerShape "ELLIPSE";
_markerArea setMarkerBrush "Solid";
_markerArea setMarkerColor markerColorEnemy;
_markerArea setMarkerSize [100, 100];
_markerArea setMarkerAlpha 0;

// Create Composition
_composition = [cmp_compositions, 48, 3, 3, ["BUNKER", "HEADQUARTERS", "OUTPOST"]] call cmp_pickComposition;
if (isNil "_composition") then {
  diag_log "DRO: Failed to find composition for assault objective, loosening parameters";
  _composition = [cmp_compositions, 48, 2, 3, ["BUNKER", "HEADQUARTERS", "OUTPOST"]] call cmp_pickComposition;
  if (isNil "_composition") then {
    diag_log "DRO: Failed to find composition for assault objective, loosening parameters";
    _composition = [cmp_compositions, 48, 2, 3] call cmp_pickComposition;
  };
};

[_composition, _thisPos, 48] call dro_generateComposition;

// Create guards
_spawnPos = [_thisPos, 100, 250] call BIS_fnc_findSafePos;
for "_i" from 0 to 1 do {
  _minAI = round (2 * aiMultiplier);
  _maxAI = round (4 * aiMultiplier);
  _spawnedSquad = [_spawnPos, enemySide, eInfClassesForWeights, eInfClassWeights, [_minAI, _maxAI]] call dro_spawnGroupWeighted;
  if (!isNil "_spawnedSquad") then {
    diag_log "spawned";
    [_spawnedSquad, _thisPos, 300] call BIS_fnc_taskPatrol;
    _dist = 10;
  };
};

// Create Task
_taskName = format ["task%1", floor(random 100000)];
_taskTitle = "Assault Fortification";
_taskDesc = format ["Assault <marker name='%1'>fortification</marker> held by %2 troops.", _markerName, enemyFactionName];
_taskType = "assault";
missionNamespace setVariable [format ["%1Completed", _taskName], 0, true];

// Create triggers
_trgAreaClear = createTrigger ["EmptyDetector", _thisPos, true];
_trgAreaClear setTriggerArea [100, 100, 0, false, 20];
_trgAreaClear setTriggerActivation ["ANY", "PRESENT", false];
_trgAreaClear setTriggerStatements [
  "
    (({(side _x == enemySide) && alive _x} count thisList) <= 0)
  ",
  "
    (thisTrigger getVariable 'markerName') setMarkerAlpha 0;
    [(thisTrigger getVariable 'thisTask'), 'SUCCEEDED', true] spawn BIS_fnc_taskSetState;
    missionNamespace setVariable [format ['%1Completed', (thisTrigger getVariable 'thisTask')], 1, true];
  ",
  ""];
_trgAreaClear setTriggerTimeout [5, 8, 10, true];
_trgAreaClear setVariable ["markerName", _markerName, true];
_trgAreaClear setVariable ["thisTask", _taskName, true];

if (isNil "cmpIndex") then {
  cmpIndex = 0;
};

// Create Marker
_cmpmarkerName = format["compositionMarker%1", cmpIndex];
_cmpmarker = createMarker [_cmpmarkerName, _thisPos];
_cmpmarker setMarkerShape "ICON";
_cmpmarker setMarkerType "hd_warning";
_cmpmarker setMarkerText "Fortification";
_cmpmarker setMarkerColor markerColorEnemy;
_cmpmarker setMarkerAlpha 0;
enemyIntelMarkers pushBack _cmpmarker;
travelPosPOIMil pushBack _thisPos;

cmpIndex = cmpIndex + 1;

allObjectives pushBack _taskName;
objData pushBack [
  _taskName,
  _taskDesc,
  _taskTitle,
  _markerName,
  _taskType,
  _thisPos,
  (random 1)
];
diag_log format ["DRO: Task created: %1, %2", _taskTitle, _taskName];
diag_log format ["DRO: objData: %1", objData];
diag_log format ["DRO: allObjectives is now %1", allObjectives];
