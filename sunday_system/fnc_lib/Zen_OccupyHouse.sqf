// Infantry Occupy House
// by Zenophon
// Released under Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)
// http://creativecommons.org/licenses/by-nc/4.0/

// Edited by Bloodshot for usage in Dynamic Recon Ops

// Teleports the units to random windows of the building(s) within the distance
// Faces units in the right direction and orders them to stand up or crouch on a roof
// Units will only fill the building to as many positions as there are windows
// Multiple buildings can be filled either evenly or to the limit of each sequentially
// Usage : Call, execVM
// Params: 1. Array, the buildings to garrison
//         2. Array of objects, the units that will garrison the building(s)
//  (opt.) 3. Boolean, true to put units on the roof, false for only inside, (default: false)
//  (opt.) 4. Boolean, true to fill all buildings in radius evenly, false for one by one, (default: false)
//  (opt.) 5. Array, a list of custom building positions each in the following format:
//            [Class Name, Positions]
//  (opt.) 6. Array, a list of classnames of structures to always treat as "Houses"
//            These are garrisoned more often
//  (opt.) 7. Boolean, true to fill from the top of the building down, (default: false)
//  (opt.) 8. Boolean, true to order AI units to move to the position instead of teleporting, (default: false)
// Return: Array of objects, the units that were not garrisoned

#define I(X) X = X + 1;
#define EYE_HEIGHT 1.53
#define CHECK_DISTANCE 5
#define FOV_ANGLE 10
#define ROOF_CHECK 4
#define ROOF_EDGE 2

private ["_buildingsArray", "_units", "_putOnRoof", "_fillEvenly",
         "_customPositions", "_Zen_ExtendPosition", "_buildingsArray",
         "_buildingPositions", "_posArray", "_unitIndex", "_j",
         "_building", "_posArray", "_randomIndex", "_housePos", "_startAngle", "_i",
         "_checkPos", "_hitCount", "_isRoof", "_edge", "_k", "_unUsedUnits", "_array",
         "_sortHeight",  "_doMove", "_customPosArray", "_houseWeight", "_wallWeight",
         "_housePosArray", "_wallPosArray", "_houses", "_walls", "_buildingAzimuth",
         "_customHouses", "_angle"];

_buildingsArray = _this param [0, [], [[]]];
_units = _this param [1, [objNull], [[]]];
_putOnRoof = _this param [2, false, [true]];
_fillEvenly = _this param [3, false, [true]];
_customPositions = _this param [4, [], [[]]];
_customHouses = _this param [5, [], [[]]];
_sortHeight = _this param [6, false, [true]];
_doMove = _this param [7, false, [true]];

if ((count _units == 0) || {isNull (_units select 0)}) exitWith {
    diag_log "Zen_Occupy House Error : No units given.";
    ([])
};

_Zen_ExtendPosition = {
    private ["_center", "_dist", "_phi"];

    _center = _this select 0;
    _dist = _this select 1;
    _phi = _this select 2;

    ([(_center select 0) + (_dist * (cos _phi)),(_center select 1) + (_dist * (sin _phi)), (_this select 3)])
};

if (count _buildingsArray == 0) exitWith {
    diag_log "Zen_Occupy House Error : No buildings found.";
    ([])
};

_houses = [];
_housePosArray = [];
_walls = [];
_wallPosArray = [];
{
    _posArray = [];
    _buildingClass = typeOf _x;
    _buildingPos = ASLtoAGL getPosASL _x;
    _buildingAzimuth = getDir _x;
    _i = _customPositions findIf {(_x select 0) == _buildingClass};

    if (_i == -1) then {
        for "_i" from 0 to 1000 do {
            if ((_x buildingPos _i) isEqualTo [0,0,0]) exitWith {};
            _posArray pushBack (_x buildingPos _i);
        };
    } else {
        _posArray = ((_customPositions select _i) select 1) apply {
            ([_x, -_buildingAzimuth] call BIS_fnc_rotateVector2D) vectorAdd _buildingPos;
        };
    };

    if (count _posArray > 0) then {
        if (_x isKindOf "House" || _buildingClass in _customHouses) then {
            _houses pushBack _x;
            _housePosArray pushBack _posArray;
        } else {
            _walls pushBack _x;
            _wallPosArray pushBack _posArray;
        };
    };
} forEach _buildingsArray;

if (_sortHeight) then {
    _housePosArray =_housePosArray apply {
        [_x, [], {_this select 2}, "DESCEND"] call BIS_fnc_sortBy
    };
} else {
    _housePosArray = _housePosArray apply {
        _x call BIS_fnc_arrayShuffle
    };
};

_wallPosArray = _wallPosArray call BIS_fnc_arrayShuffle;

_unitIndex = 0;
_houseIndex = 0;
_wallIndex = 0;
while {(_unitIndex < count _units) && {(count _housePosArray + count _wallPosArray) > 0}} do {
    scopeName "outer";

    // This weight controls how likely a soldier is to garrison a
    // structure as opposed to a wall or other non-structure
    // 0.8 corresponds to 80%
    _houseWeight = 0.8;
    _posArray = nil;
    _building = nil;

    if ((count _housePosArray > 0) && (random 1 < _houseWeight || count _wallPosArray == 0)) then {
        _posArray = _housePosArray select (_houseIndex % count _housePosArray);
        if (count _posArray == 0) then {
            _housePosArray deleteAt (_houseIndex % (count _housePosArray));
        } else {
            _building = _houses select (_houseIndex % count _houses);
            I(_houseIndex)
        };
    } else {
        _posArray = _wallPosArray select (_wallIndex % count _wallPosArray);
        if (count _posArray == 0) then {
            _wallPosArray deleteAt (_wallIndex % (count _wallPosArray));
        } else {
            _building = _walls select (_wallIndex % count _walls);
            I(_wallIndex)
        };
    };

    while {(count _posArray) > 0} do {
        scopeName "inner";
        if (_unitIndex >= count _units) exitWith {};

        _housePos = _posArray select 0;
        _posArray deleteAt 0;
        _housePos = [(_housePos select 0), (_housePos select 1), (_housePos select 2) + (getTerrainHeightASL _housePos) + EYE_HEIGHT];

        // Check for clutter in the way
        if (count ((_housePos nearObjects 3) - [_building]) > 0) then {
            diag_log format ["Zen_Occupy House : Skipping placing unit at %1; objects are in the way.", _housePos];
            breakTo "inner";
        };

        _startAngle = round random 360;
        _angle = for "_i" from _startAngle to (_startAngle + 350) step 10 do {
            _checkPos = [_housePos, CHECK_DISTANCE, (90 - _i), (_housePos select 2)] call _Zen_ExtendPosition;
            if !(lineIntersects [_checkPos, [_checkPos select 0, _checkPos select 1, (_checkPos select 2) + 25], objNull, objNull]) then {
                if !(lineIntersects [_housePos, _checkPos, objNull, objNull]) then {
                    _checkPos = [_housePos, CHECK_DISTANCE, (90 - _i), (_housePos select 2) + (CHECK_DISTANCE * tan FOV_ANGLE)] call _Zen_ExtendPosition;
                    if !(lineIntersects [_housePos, _checkPos, objNull, objNull]) exitWith {_i};
                };
            };
            _startAngle
        };

        _hitCount = 0;
        for "_k" from 30 to 360 step 30 do {
            _checkPos = [_housePos, 20, (90 - _k), (_housePos select 2)] call _Zen_ExtendPosition;
            if (lineIntersects [_housePos, _checkPos, objNull, objNull]) then {
                I(_hitCount)
            };

            if (_hitCount >= ROOF_CHECK) exitWith {};
        };

        _isRoof = (_hitCount < ROOF_CHECK) && {!(lineIntersects [_housePos, [_housePos select 0, _housePos select 1, (_housePos select 2) + 25], objNull, objNull])};
        if (!(_isRoof) || {((_isRoof) && {(_putOnRoof)})}) then {
            if (_isRoof) then {
                _edge = false;
                for "_k" from 30 to 360 step 30 do {
                    _checkPos = [_housePos, ROOF_EDGE, (90 - _k), (_housePos select 2)] call _Zen_ExtendPosition;
                    _edge = !(lineIntersects [_checkPos, [(_checkPos select 0), (_checkPos select 1), (_checkPos select 2) - EYE_HEIGHT - 1], objNull, objNull]);

                    if (_edge) exitWith {
                        _angle = _k;
                    };
                };
            };

            if (!(_isRoof) || {_edge}) then {
                (_units select _unitIndex) doWatch ([_housePos, CHECK_DISTANCE, (90 - _angle), (_housePos select 2) - (getTerrainHeightASL _housePos)] call _Zen_ExtendPosition);

                (_units select _unitIndex) disableAI "TARGET";
                if (_doMove) then {
                    (_units select _unitIndex) doMove ASLToATL ([(_housePos select 0), (_housePos select 1), (_housePos select 2) - EYE_HEIGHT]);
                } else {
                    (_units select _unitIndex) setPosASL [(_housePos select 0), (_housePos select 1), (_housePos select 2) - EYE_HEIGHT];
                    (_units select _unitIndex) setDir _angle;

                    doStop (_units select _unitIndex);
                    (_units select _unitIndex) forceSpeed 0;
                };

               //** JBOY_UpDown by JohnnyBoy //*/
                #define JBOY_UpDown \
                    if (!isServer)  exitWith {}; \
                    _dude = _this select 0; \
                    _stances = _this select 1; \
                    _dude removeAllEventHandlers "FiredNear"; \
                    while {alive _dude} do { \
                        if ((unitPos _dude) == (_stances select 0)) then { \
                            _dude setUnitPos (_stances select 1); \
                        } else { \
                            _dude setUnitPos (_stances select 0); \
                        }; \
                        sleep (1 + (random 7)); \
                    };

                if (_isRoof) then {
                    (_units select _unitIndex) setUnitPos "MIDDLE";
                   (_units select _unitIndex) addEventHandler ["FiredNear",{[(_this select 0),["DOWN","MIDDLE"]] spawn {JBOY_UpDown};}];
                } else {
                    (_units select _unitIndex) setUnitPos "UP";
                   (_units select _unitIndex) addEventHandler ["FiredNear",{[(_this select 0),["UP","MIDDLE"]] spawn {JBOY_UpDown};}];
                };

                I(_unitIndex)
                if (_fillEvenly) then {
                    breakTo "outer";
                } else {
                    breakTo "inner";
                };
            };
        };
    };
};

if (_doMove) then {
    0 = [_units, _unitIndex] spawn {
        _units = _this select 0;
        _unitIndex = _this select 1;

        _usedUnits = [];
        for "_i" from 0 to (_unitIndex - 1) do {
            _usedUnits pushBack (_units select _i);
        };

        while {count _usedUnits > 0} do {
            sleep 1;
            _toRemove =  [];
            {
                if (unitReady _x) then {
                    doStop _x;
                    _x forceSpeed 0;
                    _toRemove pushBack _forEachIndex;
                };
            } forEach _usedUnits;

            {
                _usedUnits deleteAt (_x - _forEachIndex);
            } forEach _toRemove;
        };
        if (true) exitWith {};
    };
};

_unUsedUnits = [];
for "_i" from _unitIndex to (count _units - 1) step 1 do {
    _unUsedUnits pushBack (_units select _i);
};

(_unUsedUnits)
