params ["_AOIndex", "_AOSize", "_compositionPool"];

diag_log format ["DRO: Spending %1 points to place compositions", _compositionPool];

_ao = AOLocations select _AOIndex;
_aopositions = _ao select 2;
_roads = (_aopositions select 0) + (_aopositions select 1);
_ground = (_aopositions select 2) + (_aopositions select 3) +
          (_aopositions select 4) + (_aopositions select 5);
_forest = (_aopositions select 6);

// Filter out ground positions that have roads
_ground = _ground select {count (_x nearRoads 25) == 0};

_failedAttempts = 0;
_blacklist = [];

if (isNil "cmpIndex") then {
  cmpIndex = 0;
};

while {_compositionPool > 0 && _failedAttempts < 4} do {
  _compositions = selectRandomWeighted [cmp_bunkers, 1, cmp_headquarters, 0.1, cmp_checkpoints, 0.4, cmp_emplacements, 1];
  _composition = [_compositions, _compositionPool] call cmp_pickComposition;

  if (isNil "_composition") then {
    diag_log format ["DRO: Failed to select composition from pool"];
    _failedAttempts = _failedAttempts + 1;
  } else {
    _compositionSize = _composition select 1;
    _compositionType = _composition select 4;
    _compositionName = nil;
    _posary = nil;

    switch(_compositionType) do { // Composition type
      case "BUNKER": {
        _posary = selectRandomWeighted [_ground, 1, _forest, 0.3];
        _compositionName = "Bunker";
      };

      case "CHECKPOINT": {
        _posary = _roads;
        _compositionName = "Checkpoint";
      };

      case "EMPLACEMENT": {
        _posary = selectRandomWeighted [_ground, 1, _forest, 0.8];
        _compositionName = "Emplacement";
      };

      case "HEADQUARTERS": {
        _posary = _ground;
        _compositionName = "Headquarters";
      };

      case "OUTPOST": {
        _posary = selectRandomWeighted [_ground, 1, _forest, 0.2];
        _compositionName = "Outpost";
      };

      case "CAMP": {
        _posary = selectRandomWeighted [_ground, 0.5, _forest, 1];
        _compositionName = "Camp";
      };

      default {
        _posary = _ground;
        _compositionName = "";
      };
    };

    _pos = [_posary, _compositionSize, _blacklist] call cmp_pickEmptyPosition;
    if (!isNil "_pos") then {
      diag_log format ["DRO: Placing composition of type %1 and size %2", _compositionName, _compositionSize];

      _cpoints = [_composition, _pos, _compositionPool] call dro_generateComposition;
      _compositionPool = _compositionPool - _cpoints;
      diag_log format ["DRO: Spent %1 points, %2 left", _cpoints, _compositionPool];

      _blacklist pushBack _pos;

      // Create Marker
      _markerName = format["compositionMarker%1", cmpIndex];
      _marker = createMarker [_markerName, _pos];
      _marker setMarkerShape "ICON";
      _marker setMarkerType "hd_warning";
      _marker setMarkerText _compositionName;
      _marker setMarkerColor markerColorEnemy;
      _marker setMarkerAlpha 0;
      enemyIntelMarkers pushBack _marker;
      travelPosPOIMil pushBack _pos;

      cmpIndex = cmpIndex + 1;
    } else {
      diag_log format ["DRO: Failed to place composition of type %1 and size %2", _compositionName, _compositionSize];
      _failedAttempts = _failedAttempts + 1;
    };
  };
};
